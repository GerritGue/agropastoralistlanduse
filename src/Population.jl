# Copyright (C) 2021, Gerrit Günther <guenther@geographie.uni-kiel.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# 1 tick is equal to 6 hours; 1 year = 1460 ticks

population_df = DataFrame(CSV.File(datadir("InputData/myPopulation.csv"); types = Dict(:Sex => Symbol, :ActivityLevel => Symbol)))

@agent Population GridAgent{2} begin
    population_id::Int64
    home::Int64
    age::Float64
    sex::Symbol
    activity_level::Symbol
    kcal_req_day::Float64
    energy::Float64
    max_energy::Float64
	gave_birth::Bool
    number_births_given::Float64
    pregnant::Bool
    gestation_period::Float64
    days_since_last_birth::Float64
    amount_goat_meat_kg_consumption_person::Float64
	amount_sheep_meat_kg_consumption_person::Float64
	amount_goat_cheese_kg_consumption_person::Float64
	amount_sheep_cheese_kg_consumption_person::Float64	
	prop_goat_meat_cons::Float64
	prop_sheep_meat_cons::Float64
	prop_goat_cheese_cons::Float64
	prop_sheep_cheese_cons::Float64
	goat_meat_kcal_consumed::Float64
	goat_cheese_kcal_consumed::Float64
	sheep_meat_kcal_consumed::Float64
	sheep_cheese_kcal_consumed::Float64
	goat_meat_consumed::Bool
	goat_cheese_consumed::Bool
	sheep_meat_consumed::Bool
	sheep_cheese_consumed::Bool
	simulated_days::Float64
	daily_kcal_req::Float64
	daily_kcal_cons::Float64

end

function agent_step!(population::Population, model)

	population.age += (0.0027397272 / 4) # agents age 1/365 per day or (1/365) / 4 per tick 

	agents_at_pos = collect(agents_in_position(population, model))

    settlements = filter!(x -> isa(x, Settlement), agents_at_pos)

    home_settlement = filter(settlement -> settlement.settlement_id == population.home, settlements)
    
    if model.tick == 0

		check_kcal_req_day!(population, model)	
		
		eat_goat_products!(population, model)
		eat_sheep_products!(population, model)

        population.daily_kcal_req = population.kcal_req_day * (
			population.prop_goat_meat_cons + 
			population.prop_goat_cheese_cons + 
			population.prop_sheep_meat_cons + 
			population.prop_sheep_cheese_cons
		)

		population.daily_kcal_cons = (
			population.goat_meat_kcal_consumed + 
			population.goat_cheese_kcal_consumed +
			population.sheep_meat_kcal_consumed + 
			population.sheep_cheese_kcal_consumed
		)

		push!(population_data_df.tick, model.tick)
		push!(population_data_df.year, model.year)
		push!(population_data_df.id, population.population_id)
		push!(population_data_df.age, population.age)
		push!(population_data_df.sex, population.sex)
		push!(population_data_df.energy, population.energy)
		push!(population_data_df.activity_level, population.activity_level)
		push!(population_data_df.kcal_req, population.kcal_req_day)
		push!(population_data_df.goat_meat_cons, population.goat_meat_kcal_consumed)
		push!(population_data_df.goat_cheese_cons, population.goat_cheese_kcal_consumed)
		push!(population_data_df.sheep_meat_cons, population.sheep_meat_kcal_consumed)
		push!(population_data_df.sheep_cheese_cons, population.sheep_cheese_kcal_consumed)
		push!(population_data_df.daily_kcal_cons, population.daily_kcal_cons)
		push!(population_data_df.daily_kcal_req, population.daily_kcal_req)
		push!(population_data_df.pregnant, population.pregnant)
		push!(population_data_df.gestation_period, population.gestation_period)
		push!(population_data_df.days_since_last_birth, population.days_since_last_birth)
		push!(population_data_df.number_births_given, population.number_births_given)


		if population.daily_kcal_cons > population.daily_kcal_req * 0.75
			
			if population.energy <= population.max_energy
				population.energy += 1
			end

			population.daily_kcal_cons = 0

			reset_consumed_products!(population, model)
		
		elseif population.daily_kcal_cons > (population.daily_kcal_req * 0.25) && population.daily_kcal_cons < (population.daily_kcal_req * 0.75)
			
			population.daily_kcal_cons = 0

			reset_consumed_products!(population, model)
	
		else
			population.energy -= 1

			population.daily_kcal_cons = 0

			reset_consumed_products!(population, model)
		end
	end

	## human reproduction

	if population.sex == :female && population.age >= 14

		pregnancy = rand(model.rng, 1:70)

		if pregnancy == 2 && population.pregnant == false && population.gave_birth == false && home_settlement[1].population < home_settlement[1].final_max_population * 0.8 && population.energy >= population.max_energy * 0.8 

			population.days_since_last_birth = 0

			population.pregnant = true ## agent is pregnant

			population.kcal_req_day += 400 ## kcal req increase due to pregnancy

		end

		if population.pregnant == true

			population.gestation_period += 0.25 ## gestation_period increases

		end
	
		if population.gestation_period >= 270 + rand(model.rng, 0:20) - rand(model.rng, 0:20) ## c. 9 months of pregnancy

			birth_or_death = rand(model.rng, 1:100) ## 9 % chance of stillbirth or miscarriages -> https://www.jstor.org/stable/4603062 page 390

			if birth_or_death > 9 

				spawn_population!(population, model)

				population.number_births_given += 1

				population.gave_birth = true ## gave birth

				population.pregnant = false ## not pregnant anymore
	
				population.gestation_period = 0

			else
				kill_agent!(population, model) ## stillbirth or miscarriages
			end
		end

		if population.gestation_period >= 310 + rand(model.rng, 0:20) - rand(model.rng, 0:20)
			
			population.pregnant = false ## not pregnant anymore, but did not die
	
			population.gestation_period = 0

			population.days_since_last_birth = 0
		end

		if population.gave_birth == true || population.pregnant == false
			population.days_since_last_birth += 0.25
		end
		
		if population.days_since_last_birth == 540 + rand(model.rng, 0:120) - rand(model.rng, 0:120)
			population.gave_birth = false
		end
	end
	
	## agents die at a certain age

	if population.age > 40 + rand(model.rng, 1:5) - rand(model.rng, 1:5) # change back to 40 ######################
		kill_agent!(population, model)
	end

end

function check_kcal_req_day!(agent, model)

	##	switch activity_level

	if agent.age >= 18
		rand_activity = rand(model.rng, 1:3)

		if rand_activity == 1
			agent.activity_level = :sedentary
		elseif rand_activity == 2
			agent.activity_level = :moderately_active
		else
			agent.activity_level = :active
		end
	end

	## 	kcal req based on age and activity level

	if agent.age <= 2
		agent.kcal_req_day = 1000
	end

	if agent.sex == :female && agent.activity_level == :active
	
		if agent.age >= 3 && agent.age <= 4
			agent.kcal_req_day = 1400
		elseif agent.age >= 4 && agent.age <= 6
			agent.kcal_req_day = 1600
		elseif agent.age >= 6 && agent.age <= 9
			agent.kcal_req_day = 1800
		elseif agent.age >= 9 && agent.age <= 11
			agent.kcal_req_day = 2000
		elseif agent.age >= 11 && agent.age <= 13
			agent.kcal_req_day = 2200
		elseif agent.age >= 13 && agent.age <= 30
			agent.kcal_req_day = 2400
		elseif agent.age >= 30 && agent.age <= 60
			agent.kcal_req_day = 2200
		else 
			agent.kcal_req_day = 2000
		end

	elseif agent.sex == :female && agent.activity_level == :moderately_active

		if agent.age >= 3 && agent.age <= 4
			agent.kcal_req_day = 1200
		elseif agent.age >= 4 && agent.age <= 6
			agent.kcal_req_day = 1400
		elseif agent.age >= 6 && agent.age <= 9
			agent.kcal_req_day = 1600
		elseif agent.age >= 9 && agent.age <= 11
			agent.kcal_req_day = 1800
		elseif agent.age >= 11 && agent.age <= 18
			agent.kcal_req_day = 2000
		elseif agent.age >= 11 && agent.age <= 18
			agent.kcal_req_day = 2000
		elseif agent.age >= 18 && agent.age <= 25
			agent.kcal_req_day = 2200
		elseif agent.age >= 25 && agent.age <= 50
			agent.kcal_req_day = 2000
		else
			agent.kcal_req_day = 1800
		end

	elseif agent.sex == :female && agent.activity_level == :sedentary

		if agent.age <= 4 
			agent.kcal_req_day = 1000
		elseif agent.age >= 4 && agent.age <= 7
			agent.kcal_req_day = 1200
		elseif agent.age >= 7 && agent.age <= 10
			agent.kcal_req_day = 1400
		elseif agent.age >= 10 && agent.age <= 13
			agent.kcal_req_day = 1600
		elseif agent.age >= 13 && agent.age <= 18
			agent.kcal_req_day = 1800
		elseif agent.age >= 18 && agent.age <= 25
			agent.kcal_req_day = 2000
		elseif agent.age >= 25 && agent.age <= 50
			agent.kcal_req_day = 1800
		else
			agent.kcal_req_day = 1600
		end
		
	elseif agent.sex == :male && agent.activity_level == :active

		if agent.age >= 2 && agent.age <= 3
			agent.kcal_req_day = 1400
		elseif agent.age >= 3 && agent.age <= 5
			agent.kcal_req_day = 1600
		elseif agent.age >= 5 && agent.age <= 7
			agent.kcal_req_day = 1800
		elseif agent.age >= 7 && agent.age <= 9
			agent.kcal_req_day = 2000
		elseif agent.age >= 9 && agent.age <= 11
			agent.kcal_req_day = 2200
		elseif agent.age >= 11 && agent.age <= 13
			agent.kcal_req_day = 2400
		elseif agent.age >= 13 && agent.age <= 14
			agent.kcal_req_day = 2600
		elseif agent.age >= 14 && agent.age <= 15
			agent.kcal_req_day = 2800
		elseif agent.age >= 15 && agent.age <= 16
			agent.kcal_req_day = 3000
		elseif agent.age >= 16 && agent.age <= 18
			agent.kcal_req_day = 3200
		elseif agent.age >= 18 && agent.age <= 35
			agent.kcal_req_day = 3000
		elseif agent.age >= 35 && agent.age <= 55
			agent.kcal_req_day = 2800
		else 
			agent.kcal_req_day = 2600
		end

	elseif agent.sex == :male && agent.activity_level == :moderately_active

		if agent.age >= 2 && agent.age <= 3
			agent.kcal_req_day = 1000
		elseif agent.age >= 3 && agent.age <= 5
			agent.kcal_req_day = 1400
		elseif agent.age >= 5 && agent.age <= 8
			agent.kcal_req_day = 1600
		elseif agent.age >= 8 && agent.age <= 10
			agent.kcal_req_day = 1800
		elseif agent.age >= 10 && agent.age <= 12
			agent.kcal_req_day = 2000
		elseif agent.age >= 12 && agent.age <= 13
			agent.kcal_req_day = 2200
		elseif agent.age >= 13 && agent.age <= 15
			agent.kcal_req_day = 2400
		elseif agent.age >= 15 && agent.age <= 16
			agent.kcal_req_day = 2600
		elseif agent.age >= 16 && agent.age <= 25
			agent.kcal_req_day = 2800
		elseif agent.age >= 25 && agent.age <= 45
			agent.kcal_req_day = 2600
		elseif agent.age >= 45 && agent.age <= 65
			agent.kcal_req_day = 2400
		else
			agent.kcal_req_day = 2200
		end

	elseif agent.sex == :male && agent.activity_level == :sedentary

		if agent.age >= 2 && agent.age <= 5
			agent.kcal_req_day = 1200
		elseif agent.age >= 5 && agent.age <= 8
			agent.kcal_req_day = 1400
		elseif agent.age >= 8 && agent.age <= 10
			agent.kcal_req_day = 1600
		elseif agent.age >= 10 && agent.age <= 12
			agent.kcal_req_day = 1800
		elseif agent.age >= 12 && agent.age <= 14
			agent.kcal_req_day = 2000
		elseif agent.age >= 14 && agent.age <= 15
			agent.kcal_req_day = 2200
		elseif agent.age >= 15 && agent.age <= 18
			agent.kcal_req_day = 2400
		elseif agent.age >= 18 && agent.age <= 20
			agent.kcal_req_day = 2600
		elseif agent.age >= 20 && agent.age <= 40
			agent.kcal_req_day = 2400
		elseif agent.age >= 40 && agent.age <= 60
			agent.kcal_req_day = 2200
		else
			agent.kcal_req_day = 2000
		end
	end	
end

### eating

function eat_goat_products!(agent, model)

	goat_meat_kcal_day_req_person = agent.kcal_req_day * agent.prop_goat_meat_cons 
		
	agent.amount_goat_meat_kg_consumption_person = ((goat_meat_kcal_day_req_person / goat_meat_kcal_density) / 1000) # divide by 1000 to "transform" the result from g to kg
				
	goat_cheese_kcal_day_req_person = agent.kcal_req_day * agent.prop_goat_cheese_cons
		
	agent.amount_goat_cheese_kg_consumption_person = ((goat_cheese_kcal_day_req_person / goat_cheese_kcal_density) / 1000)
	

	## define home settlement

	agents_at_pos = collect(agents_in_position(agent, model))

    settlements = filter!(x -> isa(x, Settlement), agents_at_pos)

    home_settlement = filter(settlement -> settlement.settlement_id == agent.home, settlements)


	## eating single products

	### goat meat

		if agent.daily_kcal_cons < agent.daily_kcal_req && agent.goat_meat_consumed == false && (agent.kcal_req_day * agent.prop_goat_meat_cons) > agent.goat_meat_kcal_consumed && home_settlement[1].goat_meat_storage_kg > agent.amount_goat_meat_kg_consumption_person
		
			home_settlement[1].goat_meat_storage_kg -= agent.amount_goat_meat_kg_consumption_person

			agent.goat_meat_kcal_consumed += agent.kcal_req_day * agent.prop_goat_meat_cons

			agent.goat_meat_consumed = true
		end

	### goat cheese

		if agent.daily_kcal_cons < agent.daily_kcal_req && agent.goat_cheese_consumed == false && (agent.kcal_req_day * agent.prop_goat_cheese_cons) > agent.goat_cheese_kcal_consumed && home_settlement[1].goat_cheese_storage_kg > agent.amount_goat_cheese_kg_consumption_person
		
			home_settlement[1].goat_cheese_storage_kg -= agent.amount_goat_cheese_kg_consumption_person

			agent.goat_cheese_kcal_consumed += agent.kcal_req_day * agent.prop_goat_cheese_cons

			agent.goat_cheese_consumed = true
		end

	## goat meat instead of goat cheese

	if agent.daily_kcal_cons < agent.daily_kcal_req && agent.goat_cheese_consumed == false && home_settlement[1].goat_cheese_storage_kg <= home_settlement[1].goat_meat_storage_kg && home_settlement[1].goat_meat_storage_kg > agent.amount_goat_meat_kg_consumption_person * 2.545455

		agent.amount_goat_meat_kg_consumption_person = agent.amount_goat_meat_kg_consumption_person * 2.545455
	
		home_settlement[1].goat_meat_storage_kg -= agent.amount_goat_meat_kg_consumption_person
	
		agent.goat_meat_kcal_consumed += agent.kcal_req_day * agent.prop_goat_cheese_cons
	
		agent.goat_cheese_consumed = true
	end

	## goat cheese instead of goat meat

	if agent.daily_kcal_cons < agent.daily_kcal_req && agent.goat_meat_consumed == false && home_settlement[1].goat_meat_storage_kg <= home_settlement[1].goat_cheese_storage_kg && home_settlement[1].goat_cheese_storage_kg > agent.amount_goat_cheese_kg_consumption_person * 0.39285714285714285

		agent.amount_goat_cheese_kg_consumption_person = agent.amount_goat_cheese_kg_consumption_person * 0.39285714285714285
	
		home_settlement[1].goat_cheese_storage_kg -= agent.amount_goat_cheese_kg_consumption_person
	
		agent.goat_cheese_kcal_consumed += agent.kcal_req_day * agent.prop_goat_cheese_cons
	
		agent.goat_meat_consumed = true
	end

end

function eat_sheep_products!(agent, model)

	sheep_meat_kcal_day_req_person = agent.kcal_req_day * agent.prop_sheep_meat_cons 
		
	agent.amount_sheep_meat_kg_consumption_person = ((sheep_meat_kcal_day_req_person / sheep_meat_kcal_density) / 1000) # divide by 1000 to "transform" the result from g to kg
				
	sheep_cheese_kcal_day_req_person = agent.kcal_req_day * agent.prop_sheep_cheese_cons
		
	agent.amount_sheep_cheese_kg_consumption_person = ((sheep_cheese_kcal_day_req_person / sheep_cheese_kcal_density) / 1000)
	
	## define home settlement

	agents_at_pos = collect(agents_in_position(agent, model))

    settlements = filter!(x -> isa(x, Settlement), agents_at_pos)

    home_settlement = filter(settlement -> settlement.settlement_id == agent.home, settlements)

	## eating single products

	### sheep meat
	
		if agent.daily_kcal_cons < agent.daily_kcal_req && agent.sheep_meat_consumed == false && (agent.kcal_req_day * agent.prop_sheep_meat_cons) > agent.sheep_meat_kcal_consumed && home_settlement[1].sheep_meat_storage_kg > agent.amount_sheep_meat_kg_consumption_person
		
			home_settlement[1].sheep_meat_storage_kg -= agent.amount_sheep_meat_kg_consumption_person

			agent.sheep_meat_kcal_consumed += agent.kcal_req_day * agent.prop_sheep_meat_cons

			agent.sheep_meat_consumed = true
		end

	### sheep cheese

		if agent.daily_kcal_cons < agent.daily_kcal_req && agent.sheep_cheese_consumed == false && (agent.kcal_req_day * agent.prop_sheep_cheese_cons) > agent.sheep_cheese_kcal_consumed && home_settlement[1].sheep_cheese_storage_kg > agent.amount_sheep_cheese_kg_consumption_person
		
			home_settlement[1].sheep_cheese_storage_kg -= agent.amount_sheep_cheese_kg_consumption_person

			agent.sheep_cheese_kcal_consumed += agent.kcal_req_day * agent.prop_sheep_cheese_cons

			agent.sheep_cheese_consumed = true
		end

	## sheep meat instead of sheep cheese

	if agent.daily_kcal_cons < agent.daily_kcal_req && agent.sheep_cheese_consumed == false && home_settlement[1].sheep_cheese_storage_kg <= home_settlement[1].sheep_meat_storage_kg && home_settlement[1].sheep_meat_storage_kg > agent.amount_sheep_meat_kg_consumption_person * 1.057692

		agent.amount_sheep_meat_kg_consumption_person = agent.amount_sheep_meat_kg_consumption_person * 1.057692
	
		home_settlement[1].sheep_meat_storage_kg -= agent.amount_sheep_meat_kg_consumption_person
	
		agent.sheep_meat_kcal_consumed += agent.kcal_req_day * agent.prop_sheep_cheese_cons
	
		agent.sheep_cheese_consumed = true
	end

	## sheep cheese instead of sheep meat

	if agent.daily_kcal_cons < agent.daily_kcal_req && agent.sheep_meat_consumed == false && home_settlement[1].sheep_meat_storage_kg <= home_settlement[1].sheep_cheese_storage_kg && home_settlement[1].sheep_cheese_storage_kg > agent.amount_sheep_cheese_kg_consumption_person * 0.9454545454545454

		agent.amount_sheep_cheese_kg_consumption_person = agent.amount_sheep_cheese_kg_consumption_person * 0.9454545454545454
	
		home_settlement[1].sheep_cheese_storage_kg -= agent.amount_sheep_cheese_kg_consumption_person
	
		agent.sheep_cheese_kcal_consumed += agent.kcal_req_day * agent.prop_sheep_cheese_cons
	
		agent.sheep_meat_consumed = true
	end

end

function reset_consumed_products!(agent, model)

	agent.goat_meat_consumed = false
	agent.goat_cheese_consumed = false

	agent.sheep_meat_consumed = false
	agent.sheep_cheese_consumed = false

	agent.goat_meat_kcal_consumed = 0
	agent.goat_cheese_kcal_consumed = 0

	agent.sheep_meat_kcal_consumed = 0
	agent.sheep_cheese_kcal_consumed = 0
end

function spawn_population!(agent, model)
	id = nextid(model)
	population_id = id
	home = agent.home
	age = 0.00
	
	rand_sex = rand(model.rng, 0:1)

    if rand_sex == 0
        sex = :female
    else
        sex = :male
    end
    
	activity_level = agent.activity_level
	kcal_req_day = 0.00
	energy = 20 + rand(model.rng, 0 : 5) - rand(model.rng, 0 : 5)
	max_energy = 30 + rand(model.rng, 0 : 5) - rand(model.rng, 0 : 5)
	gave_birth = false
	number_births_given = 0
	pregnant = false
	gestation_period = 0
	days_since_last_birth = 0
	amount_goat_meat_kg_consumption_person = 0.00
	amount_sheep_meat_kg_consumption_person = 0.00
	amount_goat_cheese_kg_consumption_person = 0.00
	amount_sheep_cheese_kg_consumption_person = 0.00
	prop_goat_meat_cons = agent.prop_goat_meat_cons
	prop_sheep_meat_cons = agent.prop_sheep_meat_cons
	prop_goat_cheese_cons = agent.prop_goat_cheese_cons
	prop_sheep_cheese_cons = agent.prop_sheep_cheese_cons
	goat_meat_kcal_consumed = 0.00
	goat_cheese_kcal_consumed = 0.00
	sheep_meat_kcal_consumed = 0.00
	sheep_cheese_kcal_consumed = 0.00
	goat_meat_consumed = false
	goat_cheese_consumed = false
	sheep_meat_consumed = false
	sheep_cheese_consumed = false
	simulated_days = 0.00
	daily_kcal_req = 0.00
	daily_kcal_cons = 0.00
	population = Population(id, (0,0), population_id, home, age, sex, activity_level, kcal_req_day, energy, max_energy, gave_birth, number_births_given, pregnant, gestation_period, days_since_last_birth, amount_goat_meat_kg_consumption_person, amount_sheep_meat_kg_consumption_person, amount_goat_cheese_kg_consumption_person, amount_sheep_cheese_kg_consumption_person, prop_goat_meat_cons, prop_sheep_meat_cons, prop_goat_cheese_cons, prop_sheep_cheese_cons, goat_meat_kcal_consumed, goat_cheese_kcal_consumed, sheep_meat_kcal_consumed, sheep_cheese_kcal_consumed, goat_meat_consumed, goat_cheese_consumed, sheep_meat_consumed, sheep_cheese_consumed, simulated_days, daily_kcal_req, daily_kcal_cons)
	add_agent!(population, agent.pos, model)
end

