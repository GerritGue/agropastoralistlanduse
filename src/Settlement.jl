# Copyright (C) 2021, Gerrit Günther <guenther@geographie.uni-kiel.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# 1 tick is equal to 6 hours; 1 year = 1460 ticks

@agent Settlement GridAgent{2} begin
    settlement_id::Int64
    population::Float64
    number_herder::Int64
    final_max_population::Float64
    goat_meat_req::Float64
    goat_meat_storage_kg::Float64
    goat_milk_storage_l::Float64
    goat_cheese_storage_kg::Float64
    goat_cheese_traded_kg::Float64
    times_goat_cheese_traded::Int64
    sheep_meat_req::Float64
    sheep_meat_storage_kg::Float64
    sheep_milk_storage_l::Float64
    sheep_cheese_storage_kg::Float64
    sheep_cheese_traded_kg::Float64
    times_sheep_cheese_traded::Int64
    days_until_market::Float64
end

function agent_step!(settlement::Settlement, model)

    settlement.days_until_market -= 0.25

    settlement.goat_meat_req = settlement.population / 4

    settlement.sheep_meat_req = settlement.population / 4

    if model.tick == 0 && settlement.goat_meat_storage_kg < settlement.goat_meat_req / 2

        slaugther_goat!(settlement, model)
    end

    if model.tick == 0 && settlement.sheep_meat_storage_kg < settlement.sheep_meat_req / 2

        slaugther_sheep!(settlement, model)
    end

    if model.tick == 0
        milk_sheep!(settlement, model)

        milk_goats!(settlement, model)
    end

    if settlement.days_until_market == 0

        trade_goat_cheese!(settlement, model)

        trade_sheep_cheese!(settlement, model)

        settlement.days_until_market = 8.5
    end

    if model.tick == 0

        number_population(settlement, model)

        number_herder(settlement, model)

        push!(settlement_data_df.year, model.year)
        push!(settlement_data_df.sheep_milk_storage_l, settlement.sheep_milk_storage_l)
        push!(settlement_data_df.goat_milk_storage_l, settlement.goat_milk_storage_l)
        push!(settlement_data_df.sheep_meat_storage_kg, settlement.sheep_meat_storage_kg)
        push!(settlement_data_df.goat_meat_storage_kg, settlement.goat_meat_storage_kg)
        push!(settlement_data_df.sheep_cheese_storage_kg, settlement.sheep_cheese_storage_kg)
        push!(settlement_data_df.goat_cheese_storage_kg, settlement.goat_cheese_storage_kg)

        push!(settlement_data_df.goat_cheese_traded_kg, settlement.goat_cheese_traded_kg)
        push!(settlement_data_df.sheep_cheese_traded_kg, settlement.sheep_cheese_traded_kg)

        push!(settlement_data_df.times_goat_cheese_traded, settlement.times_goat_cheese_traded)
        push!(settlement_data_df.times_sheep_cheese_traded, settlement.times_sheep_cheese_traded)
        push!(settlement_data_df.population, settlement.population)
        push!(settlement_data_df.number_herder, settlement.number_herder)
    end

    if model.tick == 1
        
        if settlement.goat_milk_storage_l > 0
            produce_goat_cheese!(settlement, model)
        end 

        if settlement.sheep_milk_storage_l > 0
            produce_sheep_cheese!(settlement, model)
        end

        goat_cheese_ripening!(settlement, model)

        sheep_cheese_ripening!(settlement, model)

    end
end

## check population

function number_population(agent, model)

    agents_at_pos = collect(agents_in_position(agent.pos, model))

    population_at_pos = filter!(x -> isa(x, Population), agents_at_pos)

    population = size(population_at_pos)[1]

    agent.population = population
end

function number_herder(agent, model)

    agents_at_pos = collect(agents_in_position(agent.pos, model))

    herder_at_pos = filter!(x -> isa(x, Herder), agents_at_pos)

    herder = size(herder_at_pos)[1]

    agent.number_herder = herder
end

## slaughter goats

function slaugther_goat!(agent, model)

    animals_for_slaughter = []

    selected_herd = rand(model.rng, 1:agent.number_herder)

    all_agents = collect(agents_in_position(agent, model))

    all_agents_goat = filter!(x -> isa(x, Goat), all_agents)

    own_herd = filter(goat -> goat.home == agent.settlement_id && goat.sex == :male && goat.age < 0.5 && goat.herd_id == selected_herd, all_agents_goat)

    male_juveniles_for_slaughter = size(own_herd)[1]


    if male_juveniles_for_slaughter > 75
        
        animals_for_slaughter = male_juveniles_for_slaughter
    
    else isempty(animals_for_slaughter) && male_juveniles_for_slaughter <= 75

        own_herd = filter(goat -> goat.home == agent.settlement_id && goat.age > 0.5 && goat.herd_id == selected_herd, all_agents_goat)
    
        male_goats_for_slaughter = size(own_herd)[1]

        animals_for_slaughter = male_goats_for_slaughter

    end

    if animals_for_slaughter > 75

        own_herd_df = DataFrame(own_herd)
    
        row = findmax(own_herd_df.meat_yield_kg)[2]

        max_meat = own_herd_df[row, :]

        agent.goat_meat_storage_kg += max_meat[13] - (max_meat[13] * 0.1) # 10 % loss

        kill_agent!(max_meat[1], model)
    end    
end


## slaughter sheep

function slaugther_sheep!(agent, model)

    animals_for_slaughter = []

    selected_herd = rand(model.rng, 1:agent.number_herder)

    all_agents = collect(agents_in_position(agent, model))

    all_agents_sheep = filter!(x -> isa(x, Sheep), all_agents)

    own_herd = filter(sheep -> sheep.home == agent.settlement_id && sheep.sex == :male && sheep.age < 0.5 && sheep.herd_id == selected_herd, all_agents_sheep)

    male_juveniles_for_slaughter = size(own_herd)[1]

    if male_juveniles_for_slaughter > 75
        
        animals_for_slaughter = male_juveniles_for_slaughter
    
    else isempty(animals_for_slaughter) && male_juveniles_for_slaughter <= 75

        own_herd = filter(sheep -> sheep.home == agent.settlement_id && sheep.age > 0.5 && sheep.herd_id == selected_herd, all_agents_sheep)
    
        male_sheep_for_slaughter = size(own_herd)[1]

        animals_for_slaughter = male_sheep_for_slaughter

    end

    if animals_for_slaughter > 75

        own_herd_df = DataFrame(own_herd)
    
        row = findmax(own_herd_df.meat_yield_kg)[2]

        max_meat = own_herd_df[row, :]

        agent.sheep_meat_storage_kg += max_meat[13] - (max_meat[13] * 0.1) # 10 % loss

        kill_agent!(max_meat[1], model)
    end    
end


function milk_sheep!(agent, model)

    all_agents = collect(agents_in_position(agent, model))

    all_agents_sheep = filter!(x -> isa(x, Sheep), all_agents)

    sheep_for_milking = filter(sheep -> sheep.home == agent.settlement_id && sheep.sex == :female && sheep.total_amount_of_milk_l > 0.0, all_agents_sheep)

    number_sheep_milked = 0

    if !isempty(sheep_for_milking)
          
        for i in 1:size(sheep_for_milking)[1]
        
            while number_sheep_milked < 50

                agent.sheep_milk_storage_l += sheep_for_milking[i].total_amount_of_milk_l  - (sheep_for_milking[i].total_amount_of_milk_l * 0.1)

                sheep_for_milking[i].total_amount_of_milk_l = 0.00

                sheep_for_milking[i].milked = true
     
                number_sheep_milked += 1
            end
        end
    end
end

## milk goats

function milk_goats!(agent, model)

    all_agents = collect(agents_in_position(agent, model))

    all_agents_goat = filter!(x -> isa(x, Goat), all_agents)

    goat_for_milking = filter(goat -> goat.home == agent.settlement_id && goat.sex == :female && goat.total_amount_of_milk_l > 0.0, all_agents_goat)

    number_goat_milked = 0

    if !isempty(goat_for_milking)

        for i in 1:size(goat_for_milking)[1]
        
            while number_goat_milked < 50

                agent.goat_milk_storage_l += goat_for_milking[i].total_amount_of_milk_l - (goat_for_milking[i].total_amount_of_milk_l * 0.1)

                goat_for_milking[i].total_amount_of_milk_l = 0.00

                goat_for_milking[i].milked = true
     
                number_goat_milked += 1
            end
        end
    end
end


## produce cheese

function produce_goat_cheese!(agent, model)

    if agent.goat_milk_storage_l > 0

        goat_milk_for_cheese = agent.goat_milk_storage_l

        agent.goat_milk_storage_l -= goat_milk_for_cheese

        total_goat_cheese = goat_milk_for_cheese * 0.2

        goat_cheese_minus_waste = total_goat_cheese - (total_goat_cheese * 0.1) ### 10 % loss

        push!(goat_cheese_in_production.year, model.year)
        push!(goat_cheese_in_production.amount, goat_cheese_minus_waste)
        push!(goat_cheese_in_production.time_until_finished, 90 + rand(model.rng, 0:90))

        goat_milk_for_cheese = 0

    end
end

function goat_cheese_ripening!(agent, model)
    
    ready_cheese = filter(cheese -> cheese.time_until_finished == 0, goat_cheese_in_production)

    if isempty(ready_cheese)

        for i in 1:size(goat_cheese_in_production)[1]
            goat_cheese_in_production[i, 3] -= 1
        end

    else

        for i in 1:size(ready_cheese)[1]    
            agent.goat_cheese_storage_kg += ready_cheese[i, 2]
        end
    
        filter!(cheese -> cheese.time_until_finished != 0, goat_cheese_in_production)
    
        filter!(stored_cheese -> stored_cheese.time_until_finished != 0, ready_cheese)
    end
end

function produce_sheep_cheese!(agent, model)

    if agent.sheep_milk_storage_l  > 0

        sheep_milk_for_cheese = agent.sheep_milk_storage_l

        agent.sheep_milk_storage_l -= sheep_milk_for_cheese

        total_sheep_cheese = sheep_milk_for_cheese * 0.2

        sheep_cheese_minus_waste = total_sheep_cheese - (total_sheep_cheese * 0.1) ### 10 % loss

        push!(sheep_cheese_in_production.year, model.year)
        push!(sheep_cheese_in_production.amount, sheep_cheese_minus_waste)
        push!(sheep_cheese_in_production.time_until_finished, 90 + rand(model.rng, 0:90))

        sheep_milk_for_cheese = 0
    end
end

function sheep_cheese_ripening!(agent, model)
    
    ready_cheese = filter(cheese -> cheese.time_until_finished == 0, sheep_cheese_in_production)

    if isempty(ready_cheese)

        for i in 1:size(sheep_cheese_in_production)[1]
            sheep_cheese_in_production[i, 3] -= 1
        end

    else

        for i in 1:size(ready_cheese)[1]    
            agent.sheep_cheese_storage_kg += ready_cheese[i, 2]
        end
    
        filter!(cheese -> cheese.time_until_finished != 0, sheep_cheese_in_production)
    
        filter!(stored_cheese -> stored_cheese.time_until_finished != 0, ready_cheese)
    end
end


## trade cheese

function trade_goat_cheese!(agent, model)

# 100 g per day and person
# 30 days storage -> 3 kg goat cheese per person in storage

    if agent.goat_cheese_storage_kg > agent.population * 3

        goat_cheese_for_trade = agent.goat_cheese_storage_kg - agent.population * 3

        agent.goat_cheese_storage_kg -= goat_cheese_for_trade

        agent.goat_cheese_traded_kg += goat_cheese_for_trade

        goat_cheese_for_trade = 0

        agent.times_goat_cheese_traded += 1

    end
end

function trade_sheep_cheese!(agent, model)

# 150 g per day and person
# 30 days storage -> 4.5 kg sheep cheese per person in storage

    if agent.sheep_cheese_storage_kg > agent.population * 4.5

        sheep_cheese_for_trade = agent.sheep_cheese_storage_kg - agent.population * 4.5

        agent.sheep_cheese_storage_kg -= sheep_cheese_for_trade

        agent.sheep_cheese_traded_kg += sheep_cheese_for_trade

        sheep_cheese_for_trade = 0

        agent.times_sheep_cheese_traded += 1

    end
end