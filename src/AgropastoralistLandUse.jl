# Copyright (C) 2021, Gerrit Günther <guenther@geographie.uni-kiel.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# 1 tick is equal to 6 hours; 1 year = 1460 ticks

using Agents, DataFrames, CSV, CSVFiles, GeoArrays

## load raster data

biomass_raster = datadir("./InputData/Biomass.tif")

biomass_matrix = GeoArrays.read(biomass_raster, masked = false)

suitability_pasture_raster = datadir("./InputData/suitability_pasture.tif")

suitability_pasture_matrix = GeoArrays.read(suitability_pasture_raster, masked = false)

## read temp data

filenames = readdir(datadir("./InputData/temperature_raster/"); join = true)

temp_data = []

for i = 1:12
    raster = GeoArrays.read(filenames[i], masked = false)
    push!(temp_data, raster)
end

## define kcal density of food_data_df

global goat_meat_kcal_density = 1.430 # kcal/g
global sheep_meat_kcal_density = 2.496 # kcal/g
global goat_cheese_kcal_density = 3.640 # kcal/g
global sheep_cheese_kcal_density = 2.640 # kcal/g

## define time of external disruptions

global first_external_disruption = 14

global second_external_disruption = 35


## load other agent types

include(srcdir("Goat.jl"))

include(srcdir("Sheep.jl"))

include(srcdir("Herder.jl"))

include(srcdir("Settlement.jl"))

include(srcdir("Population.jl"))


## set up the terrain as a basic environment

dims = (215, 124)

Base.@kwdef mutable struct Parameters
    simulation_step::BigInt = 0
	tick::Int = 0
    day::Float64 = 0.0
	month::Int64 = 1
	year::Float64 = 0.0
    times_grazed::Matrix = zeros(Int32, dims)
    suitability_pasture::Matrix = zeros(Float64, dims)
    biomass::Matrix = zeros(Float64, dims)
    max_biomass::Matrix = zeros(Float64, dims)
    taken::BitMatrix = falses(dims)
    temp_01::Matrix = zeros(Float64, dims)
    temp_02::Matrix = zeros(Float64, dims)
    temp_03::Matrix = zeros(Float64, dims)
    temp_04::Matrix = zeros(Float64, dims)
    temp_05::Matrix = zeros(Float64, dims)
    temp_06::Matrix = zeros(Float64, dims)
    temp_07::Matrix = zeros(Float64, dims)
    temp_08::Matrix = zeros(Float64, dims)
    temp_09::Matrix = zeros(Float64, dims)
    temp_10::Matrix = zeros(Float64, dims)
    temp_11::Matrix = zeros(Float64, dims)
    temp_12::Matrix = zeros(Float64, dims)
    current_temp::Matrix = zeros(Float64, dims)
end

## initialize model

function initialize_model(;
    n_herders = 1,
    n_goats = 1,
    n_sheep = 1,
    n_settlements = 1,
    n_population = 75,
    dims = dims,
    tick = 0,
    day = 0.0,
    month = 1,
    year = 0,
    model_step = 1,
    properties = Parameters(),

)

    global herder_data_df = DataFrame(tick = Float16[],
                                      year = Float16[],
                                      id = Int64[],
                                      number_goats = Int64[],
                                      number_juvenile_goats = Int64[],
                                      number_female_goats = Int64[],
                                      max_number_goats = Int64[],
                                      goat_herd_hunger = Int64[],
                                      number_sheep = Int64[],
                                      number_juvenile_sheep = Int64[],
                                      number_female_sheep = Int64[],
                                      max_number_sheep = Int64[],
                                      sheep_herd_hunger = Int64[],
                                      herd = Int64[]
                                      )

    global settlement_data_df = DataFrame(year = Float16[],
                                          sheep_milk_storage_l = Float64[],
                                          goat_milk_storage_l = Float64[],
                                          sheep_meat_storage_kg = Float64[],
                                          goat_meat_storage_kg = Float64[],
                                          sheep_cheese_storage_kg = Float64[],
                                          goat_cheese_storage_kg = Float64[],
                                          goat_cheese_traded_kg = Float64[],
                                          sheep_cheese_traded_kg = Float64[],
                                          times_goat_cheese_traded = Int64[],
                                          times_sheep_cheese_traded = Int64[],
                                          population = Float64[],
                                          number_herder = Int64[])

    global population_data_df = DataFrame(tick = Float64[],
                                          year = Float64[],
                                          id = Int64[],
                                          age = Float64[],
                                          sex = Symbol[],
                                          energy = Float16[],
                                          activity_level = Symbol[],
                                          kcal_req = Int64[],
                                          goat_meat_cons = Float64[],
                                          goat_cheese_cons = Float64[],
                                          sheep_meat_cons = Float64[],
                                          sheep_cheese_cons = Float64[],
                                          daily_kcal_cons = Float64[],
                                          daily_kcal_req = Float64[],
                                          pregnant = Bool[],
                                          gestation_period = Float64[],
                                          days_since_last_birth = Float64[],
                                          number_births_given = Float64[])
                                                                         
    global goat_cheese_in_production = DataFrame(year = Float64[],
                                                 amount = Float64[],
                                                 time_until_finished = Float64[])  

    global sheep_cheese_in_production = DataFrame(year = Float64[],
                                                  amount = Float64[],
                                                  time_until_finished = Float64[])  
                                          
space = GridSpace(dims, periodic = false)

model = ABM(Union{Settlement, Population, Herder, Goat, Sheep}, space, properties = properties, scheduler = Schedulers.by_type((Settlement, Population, Herder, Sheep, Goat), false),  warn = false)

id = 0

### settlement

    settlement_id = 0

    for _ in 1:n_settlements
        id += 1
        settlement_id += 1
        population = n_population
        number_herder = 0
        final_max_population = 200
        goat_meat_req = 10.00
        goat_meat_storage_kg = 300
        goat_milk_storage_l = 300
        goat_cheese_storage_kg = 300
        goat_cheese_traded_kg = 0
        times_goat_cheese_traded = 0
        sheep_meat_req = 10.00
        sheep_meat_storage_kg = 300
        sheep_milk_storage_l = 300
        sheep_cheese_storage_kg = 300
        sheep_cheese_traded_kg = 0
        times_sheep_cheese_traded = 0
        days_until_market = 8.5
        settlement = Settlement(id, (0,0), settlement_id, population, number_herder, final_max_population, goat_meat_req, goat_meat_storage_kg, goat_milk_storage_l, goat_cheese_storage_kg, goat_cheese_traded_kg, times_goat_cheese_traded, sheep_meat_req, sheep_meat_storage_kg, sheep_milk_storage_l, sheep_cheese_storage_kg, sheep_cheese_traded_kg, times_sheep_cheese_traded, days_until_market)
        add_agent!(settlement, (102, 36), model)
    end


    population_id = 0

    for _ in 1:n_population
        id += 1
        population_id += 1
        home = population_df[population_id, 2]
        age = population_df[population_id, 3] - rand(model.rng, 0:0.001:4) + rand(model.rng, 0:0.001:4)
        sex = population_df[population_id, 4]
        activity_level = population_df[population_id, 5]
        kcal_req_day = 0.00
        energy = 20 + rand(model.rng, 0 : 5) - rand(model.rng, 0 : 5)
        max_energy = 30 + rand(model.rng, 0 : 5) - rand(model.rng, 0 : 5)
        gave_birth = false
        number_births_given = 0 # https://www.jstor.org/stable/4603062 page 390: average number of birth 6.9

        if sex == :female && age > 14
            
            rand_pregnant = rand(model.rng, 0:100)

            if rand_pregnant == 1            
                pregnant = true
                gestation_period = rand(model.rng, 0:0.01:20)
                days_since_last_birth = rand(model.rng, 0:0.01:25)
            else
                pregnant = false
                gestation_period = 0
                days_since_last_birth = rand(model.rng, 0:0.01:50)
            end

        else
            pregnant = false
            gestation_period = 0
            days_since_last_birth = 0
        end
        amount_goat_meat_kg_consumption_person = 0.00
        amount_sheep_meat_kg_consumption_person = 0.00
        amount_goat_cheese_kg_consumption_person = 0.00
        amount_sheep_cheese_kg_consumption_person = 0.00	
        prop_goat_meat_cons = 0.03#0.06#
        prop_sheep_meat_cons = 0.01#0.02#
        prop_goat_cheese_cons = 0.03#0.06#
        prop_sheep_cheese_cons = 0.02#0.04#
        goat_meat_kcal_consumed = 0.00
        goat_cheese_kcal_consumed = 0.00
        sheep_meat_kcal_consumed = 0.00
        sheep_cheese_kcal_consumed = 0.00
        goat_meat_consumed = false
        goat_cheese_consumed = false
        sheep_meat_consumed = false
        sheep_cheese_consumed = false
        simulated_days = 0.00
        daily_kcal_req = 0.00
        daily_kcal_cons = 0.00
        population = Population(id, (0,0), population_id, home, age, sex, activity_level, kcal_req_day, energy, max_energy, gave_birth, number_births_given, pregnant, gestation_period, days_since_last_birth, amount_goat_meat_kg_consumption_person, amount_sheep_meat_kg_consumption_person, amount_goat_cheese_kg_consumption_person, amount_sheep_cheese_kg_consumption_person, prop_goat_meat_cons, prop_sheep_meat_cons, prop_goat_cheese_cons, prop_sheep_cheese_cons, goat_meat_kcal_consumed, goat_cheese_kcal_consumed, sheep_meat_kcal_consumed, sheep_cheese_kcal_consumed, goat_meat_consumed, goat_cheese_consumed, sheep_meat_consumed, sheep_cheese_consumed, simulated_days, daily_kcal_req, daily_kcal_cons)
        add_agent!(population, (102, 36), model)
    end

### n_herders

    herder_id = 0

    for _ in 1:n_herders
        id += 1
        herder_id += 1
        home = herder_df[herder_id, 2]
        herd = herder_df[herder_id, 3]
        number_goats = 0
        number_sheep = 0
        number_juvenile_goats = 0 
        number_juvenile_sheep = 0
        number_female_goats = 0
        number_female_sheep = 0
        max_number_goats = 150 + rand(model.rng, 0:50)
        max_number_sheep = 150 + rand(model.rng, 0:50)
        hunger_goat_herd = 0
        hunger_sheep_herd = 0
        ticks_at_grazing_spot = 0
        herder = Herder(id, (0,0), herder_id, home, herd, number_goats, number_sheep, number_juvenile_goats, number_juvenile_sheep, number_female_goats,number_female_sheep, max_number_goats, max_number_sheep, hunger_goat_herd, hunger_sheep_herd, ticks_at_grazing_spot)
        add_agent!(herder, (102, 36), model)
    end
    
### goats

    goat_id = 0

    for _ in 1:n_goats
        id += 1
        goat_id += 1
        herder = goat_df[goat_id, 1]
        herd_id = goat_df[goat_id, 2]
        home = goat_df[goat_id, 4]
        age = goat_df[goat_id, 5]

        if goat_df[goat_id, 6] == :female
            sex = :female 
        else
            sex = :male
        end

        energy = 20 + rand(model.rng, 0:10)
        hunger = 6 + rand(model.rng, 0:2)
        gave_birth = false

        if age < 2
            max_energy = 20 + rand(model.rng, 0:3) - rand(model.rng, 0:3)
            meat_yield_kg = 9
        else
            max_energy = 30 + rand(model.rng, 0:4) - rand(model.rng, 0:4)
            meat_yield_kg = 18
        end
 
        milk_individual_day_l = goat_df[goat_id, 10]
        total_amount_of_meat_kg = (meat_yield_kg + (energy / 10)) 
        total_amount_of_milk_l = 0

        if sex == :female && age >= 1.553424657534247
            days_since_last_birth = rand(model.rng, 0:85)
            gestation_period = rand(model.rng, 0:60)
        else
            days_since_last_birth = 0
            gestation_period = 0
        end
        
        pregnant = false
    
        lactation_days = goat_df[goat_id, 11]
        simulated_days = 0
        milking_simulated_days = 0
        milked = false
        goat = Goat(id, (0, 0), goat_id, herder, herd_id, home, age, sex, energy, hunger, gave_birth, max_energy, meat_yield_kg, milk_individual_day_l, total_amount_of_meat_kg, total_amount_of_milk_l, days_since_last_birth, gestation_period, pregnant, lactation_days, simulated_days, milking_simulated_days, milked)
        add_agent!(goat, (102, 36), model)
    end

    ### sheep

    sheep_id = 0

    for _ in 1:n_sheep
        id += 1
        sheep_id += 1
        herder = sheep_df[sheep_id, 1]
        herd_id = sheep_df[sheep_id, 2]
        home = sheep_df[sheep_id, 4]
        age = sheep_df[sheep_id, 5]
        sex = sheep_df[sheep_id, 6]
        energy = 20 + rand(model.rng, 0:10)
        hunger = 6 + rand(model.rng, 0:2)
        gave_birth = false

        if age < 2
            max_energy = 20 + rand(model.rng, 0:3) - rand(model.rng, 0:3)
            meat_yield_kg = 9
        else
            max_energy = 30 + rand(model.rng, 0:4) - rand(model.rng, 0:4)
            meat_yield_kg = 18
        end
 
        milk_individual_day_l = sheep_df[sheep_id, 10]
        total_amount_of_meat_kg = (meat_yield_kg + (energy / 10)) 
        total_amount_of_milk_l = 0

        if sex == :female && age >= 1.27
            days_since_last_birth = rand(model.rng, 0:113)
            gestation_period = rand(model.rng, 0:60)
        else
            days_since_last_birth = 0
            gestation_period = 0
        end
        
        pregnant = false
    
        lactation_days = sheep_df[sheep_id, 11]
        simulated_days = 0
        milking_simulated_days = 0
        milked = false
        sheep = Sheep(id, (0, 0), sheep_id, herder, herd_id, home, age, sex, energy, hunger, gave_birth, max_energy, meat_yield_kg, milk_individual_day_l, total_amount_of_meat_kg, total_amount_of_milk_l, days_since_last_birth, gestation_period, pregnant, lactation_days, simulated_days, milking_simulated_days, milked)
        add_agent!(sheep, (102, 36), model)
    end

    terrain_id = 0
    terrain_row = 0
    terrain_col = size(biomass_matrix)[2]

    for p in positions(model)
        id += 1

        if terrain_col >= 1
            if terrain_row < size(biomass_matrix)[1]
                terrain_row += 1
            else                   
                terrain_row = 1
                terrain_col -= 1
            end
        end

        model.times_grazed[p...] = 0

        model.suitability_pasture[p...] = suitability_pasture_matrix[terrain_row, terrain_col, 1]
                
        model.biomass[p...] = biomass_matrix[terrain_row, terrain_col, 1]

        model.max_biomass[p...] = biomass_matrix[terrain_row, terrain_col, 1]

        model.taken[p...] = false

        model.temp_01[p...] = temp_data[1][terrain_row, terrain_col, 1]
        model.temp_02[p...] = temp_data[2][terrain_row, terrain_col, 1]
        model.temp_03[p...] = temp_data[3][terrain_row, terrain_col, 1]
        model.temp_04[p...] = temp_data[4][terrain_row, terrain_col, 1]
        model.temp_05[p...] = temp_data[5][terrain_row, terrain_col, 1]
        model.temp_06[p...] = temp_data[6][terrain_row, terrain_col, 1]
        model.temp_07[p...] = temp_data[7][terrain_row, terrain_col, 1]
        model.temp_08[p...] = temp_data[8][terrain_row, terrain_col, 1]
        model.temp_09[p...] = temp_data[9][terrain_row, terrain_col, 1]
        model.temp_10[p...] = temp_data[10][terrain_row, terrain_col, 1]
        model.temp_11[p...] = temp_data[11][terrain_row, terrain_col, 1]
        model.temp_12[p...] = temp_data[12][terrain_row, terrain_col, 1]
        model.current_temp[p...] = 0
    end
       
       return  model
end

## steps

function model_step!(model)

    model.tick += 1

    model.day += 0.25

    model.year += 0.0006849315068493151

    if model.tick == 4
        model.tick = 0
    end

    if model.day == 365
        model.day = 0
        model.month = 1
    end

    if model.day >= 0 && model.day < 31
        model.month = 1
    elseif model.day == 31
        model.month = 2
    elseif model.day == 58
        model.month = 3
    elseif model.day == 89
        model.month = 4
    elseif model.day == 119
        model.month = 5
    elseif model.day == 150
        model.month = 6
    elseif model.day == 180
        model.month = 7
    elseif model.day == 211
        model.month = 8
    elseif model.day == 241
        model.month = 9
    elseif model.day == 272
        model.month = 10
    elseif model.day == 303
        model.month = 11
    elseif model.day == 333
        model.month = 12
    end

    biomass_growth(model)

    model.simulation_step += 1

    progress = round((model.simulation_step / n_ticks) * 100; sigdigits = 2)

    println("Progress: ", progress, " %")

    if model.simulation_step == n_ticks
        println("Simulation finished.")
    end

end

## biomass growth

function biomass_growth(model)

    @inbounds for p in positions(model)

        if model.month == 1 && model.tick == 0 && model.temp_01[p...] > 0
            model.current_temp[p...] = model.temp_01[p...] + rand(model.rng, 0 : model.temp_01[p...] * 0.2) - rand(model.rng, 0 : model.temp_01[p...] * 0.2)
        elseif model.month == 2 && model.tick == 0 && model.temp_01[p...] > 0
            model.current_temp[p...] = model.temp_02[p...] + rand(model.rng, 0 : model.temp_02[p...] * 0.1) - rand(model.rng, 0 : model.temp_02[p...] * 0.1)
        elseif model.month == 3 && model.tick == 0 && model.temp_01[p...] > 0
            model.current_temp[p...] = model.temp_03[p...] + rand(model.rng, 0 : model.temp_03[p...] * 0.1) - rand(model.rng, 0 : model.temp_03[p...] * 0.1)
        elseif model.month == 4 && model.tick == 0 && model.temp_01[p...] > 0
            model.current_temp[p...] = model.temp_04[p...]+ rand(model.rng, 0 : model.temp_04[p...] * 0.1) - rand(model.rng, 0 : model.temp_04[p...] * 0.1)
        elseif model.month == 5 && model.tick == 0 && model.temp_01[p...] > 0
            model.current_temp[p...] = model.temp_05[p...] + rand(model.rng, 0 : model.temp_05[p...] * 0.1) - rand(model.rng, 0 : model.temp_05[p...] * 0.1)
        elseif model.month == 6 && model.tick == 0 && model.temp_01[p...] > 0
            model.current_temp[p...] = model.temp_06[p...] + rand(model.rng, 0 : model.temp_06[p...] * 0.1) - rand(model.rng, 0 : model.temp_06[p...] * 0.1)
        elseif model.month == 7 && model.tick == 0 && model.temp_01[p...] > 0
            model.current_temp[p...] = model.temp_07[p...] + rand(model.rng, 0 : model.temp_07[p...] * 0.1) - rand(model.rng, 0 : model.temp_07[p...] * 0.1)
        elseif model.month == 8 && model.tick == 0 && model.temp_01[p...] > 0
            model.current_temp[p...] = model.temp_08[p...] + rand(model.rng, 0 : model.temp_08[p...] * 0.1) - rand(model.rng, 0 : model.temp_08[p...] * 0.1)
        elseif model.month == 9 && model.tick == 0 && model.temp_01[p...] > 0
            model.current_temp[p...] = model.temp_09[p...] + rand(model.rng, 0 : model.temp_09[p...] * 0.1) - rand(model.rng, 0 : model.temp_09[p...] * 0.1)
        elseif model.month == 10 && model.tick == 0 && model.temp_01[p...] > 0
            model.current_temp[p...] = model.temp_10[p...] + rand(model.rng, 0 : model.temp_10[p...] * 0.1) - rand(model.rng, 0 : model.temp_10[p...] * 0.1)
        elseif model.month == 11 && model.tick == 0 && model.temp_01[p...] > 0
            model.current_temp[p...] = model.temp_11[p...] + rand(model.rng, 0 : model.temp_11[p...] * 0.1) - rand(model.rng, 0 : model.temp_11[p...] * 0.1)
        elseif model.month == 12 && model.tick == 0 && model.temp_01[p...] > 0
            model.current_temp[p...] = model.temp_12[p...] + rand(model.rng, 0 : model.temp_12[p...] * 0.1) - rand(model.rng, 0 : model.temp_12[p...] * 0.1)
        end

        if model.current_temp[p...] >= 8
            if model.biomass[p...]  <= model.max_biomass[p...] * 0.015
                model.biomass[p...] += model.biomass[p...] * 0.015
            end
        end
    end
end
