# Copyright (C) 2021, Gerrit Günther <guenther@geographie.uni-kiel.de>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# 1 tick is equal to 6 hours; 1 year = 1460 ticks

herder_df = DataFrame(CSV.File(datadir("InputData/myPastoralist.csv")))

@agent Herder GridAgent{2} begin
    herder_id::Int64
    home::Int64
    herd::Int64
    number_goats::Int64
    number_sheep::Int64
    number_juvenile_goats::Int64
    number_juvenile_sheep::Int64
    number_female_goats::Int64
    number_female_sheep::Int64
    max_number_goats::Int64
    max_number_sheep::Int64
    hunger_goat_herd::Int64
    hunger_sheep_herd::Int64
    ticks_at_grazing_spot::Int8
end

function agent_step!(herder::Herder, model)

    if model.tick == 0
        herd_size(herder, model)
        store_herder_data(herder, model)
    end

    ### slaughtering to reduce the number of animals per herd to be able to 'oversee' them

    if model.tick == 0 && herder.number_sheep >= herder.max_number_sheep - (herder.max_number_sheep * 0.40)
            slaugther_sheep_pastoralist!(herder, model)
    end

    if model.tick == 0 && herder.number_goats >= herder.max_number_goats - (herder.max_number_goats * 0.40)

        slaugther_goats_pastoralist!(herder, model)

    end

    if model.tick == 1
        select_best_grazing_spot!(herder, 0.95, herder.hunger_goat_herd + herder.hunger_sheep_herd, model) 

        if model.taken[herder.pos...] == false
            model.taken[herder.pos...] = true
        end
    end

    if model.tick == 3

        if model.taken[herder.pos...] == true
            model.taken[herder.pos...] = false
        end

        move_home!(herder, model)
    end

    if model.day == 180 && model.tick == 0 ## natural death of juveniles
        natural_death!(herder, model)
    end
    
    if model.year > first_external_disruption && model.year < first_external_disruption + 1 && model.day == 100 || model.year > second_external_disruption && model.year < second_external_disruption + 1 && model.day == 100
        loss_of_animals!(herder, model)
    end

    if herder.number_goats == 0 && herder.number_sheep == 0 && model.tick == 4
        kill_agent!(herder, model)
    end
end

## store data in df 

function store_herder_data(agent, model)

    push!(herder_data_df.tick, model.tick)
    push!(herder_data_df.year, model.year)
    push!(herder_data_df.id, agent.herder_id)
    push!(herder_data_df.number_goats, agent.number_goats)
    push!(herder_data_df.number_juvenile_goats, agent.number_juvenile_goats)
    push!(herder_data_df.number_female_goats, agent.number_female_goats)
    push!(herder_data_df.max_number_goats, agent.max_number_goats)
    push!(herder_data_df.goat_herd_hunger, agent.hunger_goat_herd)
    push!(herder_data_df.number_sheep, agent.number_sheep)
    push!(herder_data_df.number_juvenile_sheep, agent.number_juvenile_sheep)
    push!(herder_data_df.number_female_sheep, agent.number_female_sheep)
    push!(herder_data_df.max_number_sheep, agent.max_number_sheep)
    push!(herder_data_df.sheep_herd_hunger, agent.hunger_sheep_herd)
    push!(herder_data_df.herd, agent.herd)

end

function herd_size(agent, model)

    all_agents = collect(allagents(model))

    all_sheep = filter(x -> isa(x, Sheep), all_agents)

    own_sheep_herd = filter(sheep -> sheep.herd_id == agent.herd && sheep.herder == agent.herder_id, all_sheep)

    agent.number_sheep = size(own_sheep_herd)[1]

    agent.hunger_sheep_herd = agent.number_sheep * 8


    own_female_sheep_herd = filter(sheep -> sheep.herd_id == agent.herd && sheep.herder == agent.herder_id && sheep.sex == :female, all_sheep)

    agent.number_female_sheep = size(own_female_sheep_herd)[1]


    own_juvenile_sheep_herd = filter(sheep -> sheep.herd_id == agent.herd && sheep.herder == agent.herder_id && sheep.age <= 0.5, all_sheep)

    agent.number_juvenile_sheep = size(own_juvenile_sheep_herd)[1]

    ## goats

    all_goats = filter(x -> isa(x, Goat), all_agents)

    own_goat_herd = filter(goat -> goat.herd_id == agent.herd && goat.herder == agent.herder_id, all_goats)

    agent.number_goats = size(own_goat_herd)[1]

    agent.hunger_goat_herd = agent.number_goats * 8

    
    own_female_goat_herd = filter(goat -> goat.herd_id == agent.herd && goat.herder == agent.herder_id && goat.sex == :female, all_goats)

    agent.number_female_goats = size(own_female_goat_herd)[1]


    own_juvenile_goat_herd = filter(goat -> goat.herd_id == agent.herd && goat.herder == agent.herder_id && goat.age <= 0.5, all_goats)

    agent.number_juvenile_goats = size(own_juvenile_goat_herd)[1]
end

## select grazing spot

function select_best_grazing_spot!(agent, suitability_pasture, hunger, model)

    pasture = findall(pasture -> pasture >= suitability_pasture, model.suitability_pasture)

    biomass = findall(biomass -> biomass > hunger, model.biomass)

    if size(biomass)[1] < size(pasture)[1]
        i = size(biomass)[1]
    else
        i = size(pasture)[1]
    end

    array = CartesianIndex[]

    for n in 1:i

        check = pasture[n] in biomass

        if check == true
            push!(array, pasture[n])
        end
    end
    
    best_suitability_pasture_df = DataFrame(pos = Dims{2}[],
                                            distance = Float64[])

    for i in 1:size(array)[1]

         pos = Tuple(array[i])

        distance = edistance(agent.pos, pos, model)
        
        push!(best_suitability_pasture_df.pos, pos)
        push!(best_suitability_pasture_df.distance, distance)

    end


    if !isempty(best_suitability_pasture_df)
    
        row = findmin(best_suitability_pasture_df.distance)[2]

        nearest_spot = best_suitability_pasture_df[row, :]

        row = findmin(best_suitability_pasture_df.distance)[2]

        nearest_spot = best_suitability_pasture_df[row, :]

        move_agent!(agent, nearest_spot.pos, model)
    else

        select_second_best_grazing_spot!(agent, suitability_pasture, hunger, model)
    end
end



function select_second_best_grazing_spot!(agent, suitability_pasture, hunger, model)

    pasture = findall(pasture -> pasture >= suitability_pasture - 0.1, model.suitability_pasture)

    biomass = findall(biomass -> biomass > hunger * 0.5, model.biomass)

    if size(biomass)[1] < size(pasture)[1]
        i = size(biomass)[1]
    else
        i = size(pasture)[1]
    end

    array = CartesianIndex[]

    for n in 1:i

        check = pasture[n] in biomass

        if check == true
            push!(array, pasture[n])
        end
    end
    
    best_suitability_pasture_df = DataFrame(pos = Dims{2}[],
                                            distance = Float64[])

    for i in 1:size(array)[1]

         pos = Tuple(array[i])

        distance = edistance(agent.pos, pos, model)
        
        push!(best_suitability_pasture_df.pos, pos)
        push!(best_suitability_pasture_df.distance, distance)

    end

    if !isempty(best_suitability_pasture_df)
        row = findmin(best_suitability_pasture_df.distance)[2]

        nearest_spot = best_suitability_pasture_df[row, :]

        move_agent!(agent, nearest_spot.pos, model)
    end    
end

## move home

function move_home!(agent, model)

    all_agents = collect(allagents(model))

    settlements = filter!(x -> isa(x, Settlement), all_agents)

    home_settlement = filter(settlement -> settlement.settlement_id == agent.home, settlements)

    home_settlement_df = DataFrame(home_settlement)[1, :]

    move_agent!(agent, home_settlement_df.pos, model)
end

## natural death of juveniles

function natural_death!(agent, model)

    all_agents = collect(allagents(model))

    all_sheep = filter(x -> isa(x, Sheep), all_agents)

    own_juvenile_sheep_herd = filter(sheep -> sheep.herd_id == agent.herd && sheep.herder == agent.herder_id && sheep.age <= 0.5, all_sheep)
    
    for i in 1:floor(Int, size(own_juvenile_sheep_herd)[1] )

        rand_death = rand(model.rng, 1:3)

        if rand_death == 1
            kill_agent!(own_juvenile_sheep_herd[i], model)
        end
    end

    all_goats = filter(x -> isa(x, Goat), all_agents)

    own_juvenile_goat_herd = filter(goat -> goat.herd_id == agent.herd && goat.herder == agent.herder_id && goat.age <= 0.5, all_goats)

    for i in 1:floor(Int, size(own_juvenile_goat_herd)[1])
        
        rand_death = rand(model.rng, 1:3)
        
        if rand_death == 1
            kill_agent!(own_juvenile_goat_herd[i], model)
        end
    end
end

## sell animals on the market

function loss_of_animals!(agent, model)

    all_agents = collect(allagents(model))

    all_sheep = filter(x -> isa(x, Sheep), all_agents)

    own_sheep_herd = filter(sheep -> sheep.herd_id == agent.herd && sheep.herder == agent.herder_id, all_sheep)

    sheep_to_sell = floor(Int, size(own_sheep_herd)[1] / 3)

    for i in 1:sheep_to_sell
        kill_agent!(own_sheep_herd[i], model)
    end

    all_goats = filter(x -> isa(x, Goat), all_agents)

    own_goat_herd = filter(goat -> goat.herd_id == agent.herd && goat.herder == agent.herder_id, all_goats)
    
    goats_to_sell = floor(Int, size(own_goat_herd)[1] / 3)
    
    for i in 1:goats_to_sell
        kill_agent!(own_goat_herd[i], model)
    end
end

## slaughter sheep

function slaugther_sheep_pastoralist!(agent, model)

    all_agents = collect(allagents(model))

    settlements = filter!(x -> isa(x, Settlement), all_agents)
    
    home_settlement = filter(settlement -> settlement.settlement_id == agent.home, settlements)

    all_agents = collect(agents_in_position(agent, model))

    all_agents_sheep = filter!(x -> isa(x, Sheep), all_agents)

    own_herd = filter(sheep -> sheep.herd_id == agent.herd && sheep.herder == agent.herder_id && sheep.sex == :male, all_agents_sheep)

    own_herd_df = DataFrame(own_herd)

    if !isempty(own_herd_df)
    
        row = findmax(own_herd_df.meat_yield_kg)[2]

        max_meat = own_herd_df[row, :]

        home_settlement[1].sheep_meat_storage_kg += max_meat[13] - (max_meat[13] - 0.1) # 10 % loss

        kill_agent!(max_meat[1], model) 
    end
end

## slaughter goats

function slaugther_goats_pastoralist!(agent, model)

    all_agents = collect(allagents(model))

    settlements = filter!(x -> isa(x, Settlement), all_agents)
    
    home_settlement = filter(settlement -> settlement.settlement_id == agent.home, settlements)

    all_agents = collect(agents_in_position(agent, model))

    all_agents_goat = filter!(x -> isa(x, Goat), all_agents)

    own_herd = filter(goat -> goat.herd_id == agent.herd && goat.herder == agent.herder_id && goat.sex == :male, all_agents_goat)

    own_herd_df = DataFrame(own_herd)

    if !isempty(own_herd_df)
    
        row = findmax(own_herd_df.meat_yield_kg)[2]

        max_meat = own_herd_df[row, :]

        home_settlement[1].goat_meat_storage_kg += max_meat[13] - (max_meat[13] - 0.1) # 10 % loss

        kill_agent!(max_meat[1], model) 
    end
end