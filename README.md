# AgropastoralistLandUse

This code base is using the Julia Language and [DrWatson](https://juliadynamics.github.io/DrWatson.jl/stable/)
to make a reproducible scientific project named
> AgropastoralistLandUse

It is authored by Gerrit Günther.

Using Julia:
- To run the simulation you need to install [Julia](https://julialang.org/) version 1.6.1 or higher
- Depending on your preferences, there are multiple ways to open .jl files and to run simulations.
- A simple way is to use an editor like [VSCodium](https://vscodium.com/) and the corresponding [extension](https://www.julia-vscode.org/). Of course you can also use Emacs, but I would not recommend using it on Windows.
- If you installed Julia, an editor and the extension you can continue to the next section. 


General things:
- If you are using Windows, you need to double backslashes “\ \” in paths instead of single slashes "/".

To (locally) reproduce this project, do the following:

0. Download this code base.
1. Open a Julia console and do:
   ```
   julia> using Pkg
   julia> Pkg.add("DrWatson") # install globally, for using `quickactivate`
   julia> Pkg.activate("path/to/this/project")
   julia> Pkg.instantiate()
   ```

This will install all necessary packages for you to be able to run the scripts and
everything should work out of the box, including correctly finding local paths.

The input data for the simulation can be found in the data/InputData folder.

The simulation itself consists of multiple scripts and functions, that can be found in the src folder.

If you are using VSCodium or VSCode, you can use the "open folder" dialogue to open the root-directory of your local copy of this repository. Then, open the 'simulation_run.jl' file in the scripts folder. To execute code and to run the simulation, select the lines 20 to 46 of the file and press CTRL + Enter to send the code to the console. If everything worked, the Julia Console / REPL should open and display the code you just sent. And now you have to wait.

The console displays a short message if the simlation is finished. Now, the results can be stored in the corresponding data/sims/ folder. To do this, select the lines 52 to 68 of the file and press CTRL + Enter to send the code to the console.
